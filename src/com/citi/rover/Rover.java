package com.citi.rover;

public class Rover {
    
    //receiveCommands and takeNextStep
    private String commands;
/*     private String x;
    private String y;  */
    private int x;
    private int y;
    private Boolean busy;
    private int direction;
    private int stepsToGo = 2;
   
    
    public Rover() {
    }
    
    StringBuilder nextSteps = new StringBuilder("MMLMRM");
    public String receiveCommands(String c)
    {
        this.commands = c;
        return c;
    }
    public String getCommands() {
        return commands;
    }

    public void setCommands(String commands) {
        this.commands = commands;
    }

    public Rover(String commands) {
        this.commands = commands;
    }

    public int getX()
    {
        return x;
    }
    public int getY(){
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    public Boolean getBusy() {
        return busy;
    }

    public void setBusy(Boolean busy) {
        this.busy = busy;
    }

   public Boolean isBusy()
   {
       if (nextSteps.length() != 0){
           return true;
       }
       return false;
   }

    public int getDirection()
    {
        System.out.println("Direction value " + direction);
        return direction;
    }
    public void setDirection(int direction) 
    {
        this.direction = direction;
     }
    
    public void takeNextStep()
    {
        /* System.out.println("Take next step");
        while(stepsToGo > 0)
        {
            y = y+1;
            stepsToGo = stepsToGo - 1;
        } */
        for(int i= 0; i < nextSteps.length(); i++)
        {
           nextSteps.deleteCharAt(i);
        }

    }



}


